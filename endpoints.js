const ddragon = 'https://ddragon.leagueoflegends.com';

module.exports = {
  Runes(region, champion) {
    return `http://${region}.op.gg/champion/${champion}/statistics/top/rune?`;
  },

  Champion(v, l) {
    return `${ddragon}/cdn/${v}/data/${l}/champion.json`;
  },
  Versions() {
    return `${ddragon}/api/versions.json`;
  },
};

