const endpoints = require('./endpoints');
const request = require('request-promise-native');
const cheerio = require('cheerio');
const fs = require('fs');
const ProgressBar = require('progress');
const stringify = require('json-stable-stringify');

const FirstNumbers = /[0-9]+/;

const runesFull = {};
const damageCompositions = {};
const skillOrders = {};
const roles = ['jungle', 'top', 'support', 'mid', 'adc'];
const statRequest = request.defaults({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
  },
  pool: { maxSockets: 5 }, // concurrent downloads
  timeout: 10000,
});

const skills = {
  1: 'Q',
  2: 'W',
  3: 'E',
  4: 'R',
};

async function getDamageComposition(champion, bar) {
  damageCompositions[champion] = {};
  return new Promise((resolve, reject) => {
    statRequest
      .get({
        url: `http://champion.gg/champion/${champion}`,
      })
      .catch(e => reject(e))
      .then((body) => {
        bar.tick({
          champion,
        });
        const $ = cheerio.load(body);
        $('.damage-dealt').each((i, el) => {
          const damagedealt = cheerio.load(el);
          damageCompositions[champion] = {
            physical:
              parseFloat(damagedealt('.physical-dmg')
                .attr('tooltip')
                .match(/[0-9]+.[0-9]+/)[0]) / 100,
            magical:
              parseFloat(damagedealt('.magic-dmg')
                .attr('tooltip')
                .match(/[0-9]+.[0-9]+/)[0]) / 100,
            trued:
              parseFloat(damagedealt('.true-dmg')
                .attr('tooltip')
                .match(/[0-9]+.[0-9]+/)[0]) / 100,
          };
        });
        skillOrders[champion] = [];
        $('.skill-order').each((i, el) => {
          skillOrders[champion][i] = [];
          const $1 = cheerio.load(el);
          $1('.skill .skill-selections').each((i2, el2) => {
            if (i2 > 0) {
              const skillSelections = cheerio.load(el2);
              skillSelections('div').each((i3, el3) => {
                if ($(el3).attr('class') === 'selected') {
                  skillOrders[champion][i][i3 - 1] = skills[i2];
                }
              });
            }
          });
        });
        resolve();
      });
  });
}

async function getRunes(region, champion, bar) {
  runesFull[champion] = {};
  const promises = [];
  for (let k = 0; k < roles.length; k += 1) {
    const role = roles[k];
    promises.push(new Promise((resolve, reject) => {
      statRequest
        .get({
          url: `https://${region}.op.gg/champion/${champion.toLowerCase()}/statistics/${role}/rune?`,
        })
        .catch(e => reject(e))
        .then((body) => {
          bar.tick({
            champion,
            role,
          });
          const $ = cheerio.load(body);
          const championSection = [];

          $('tbody tr').each((i, el) => {
            const tBodyTr = cheerio.load(el);
            const runeSection = {};
            runeSection.runes = [];
            tBodyTr('.perk-page__item--active').each((j, e2) => {
              const activePerkPage = cheerio.load(e2);
              const rune = activePerkPage('img')
                .attr('src')
                .match(FirstNumbers)[0];
              runeSection.runes[j] = rune;
            });
            tBodyTr('.fragment__summary').each((m, e3) => {
              const fragmentSummary = cheerio.load(e3);
              fragmentSummary('.fragment').each((l, e4) => {
                const runeFragment = cheerio.load(e4);
                runeSection.runes.push(runeFragment('img')
                  .attr('src')
                  .match(FirstNumbers)[0]);
              });
            });
            runeSection.pickrate = tBodyTr('.champion-stats__table__cell--pickrate')
              .text()
              .match(FirstNumbers)[0];
            runeSection.winrate = tBodyTr('.champion-stats__table__cell--winrate')
              .text()
              .match(FirstNumbers)[0];
            championSection[i] = runeSection;
          });
          runesFull[champion][role] = championSection;
          resolve();
        });
    }));
  }
  return Promise.all(promises);
}

/**
 * MAIN
 */
(async () => {
  const version = (await request.get({
    url: endpoints.Versions(),
    json: true,
  }))[0];
  const champions = await request.get({
    url: endpoints.Champion(version, 'en_US'),
    json: true,
  });
  const championIds = Object.keys(champions.data);
  const bar = new ProgressBar(':bar :percent :champion :role', {
    total: championIds.length * roles.length + championIds.length,
    width: 30,
  });

  const promises = [];
  for (let i = 0; i < championIds.length; i += 1) {
    promises.push(getRunes('euw', championIds[i], bar));
    promises.push(getDamageComposition(championIds[i], bar));
  }
  try {
    await Promise.all(promises);
    console.log('Downloads Complete');
    if (!fs.existsSync('./output')) {
      fs.mkdirSync('./output');
    }
    [
      {
        location: 'everyRunePage',
        object: runesFull,
      },
      {
        location: 'damageComposition',
        object: damageCompositions,
      },
      {
        location: 'skillOrders',
        object: skillOrders,
      },
    ].forEach(({ location, object }) => {
      fs.writeFileSync(`./output/${location}.json`, stringify(object));
      console.log(`Wrote to ./output/${location}.json`);
      fs.writeFileSync(`./output/${location}_pretty.json`, stringify(object, { space: 2 }));
      console.log(`Wrote to ./output/${location}_pretty.json`);
    });
  } catch (e) {
    console.log(e);
  }
})();
